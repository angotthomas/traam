gdjs._4990_32DOSSIERCYBERSECUCode = {};
gdjs._4990_32DOSSIERCYBERSECUCode.localVariables = [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDFondObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDFondObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDbackObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDbackObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects2= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDadresseObjects1= [];
gdjs._4990_32DOSSIERCYBERSECUCode.GDadresseObjects2= [];


gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDcontroleObjects1Objects = Hashtable.newFrom({"controle": gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects1});
gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDextObjects1Objects = Hashtable.newFrom({"ext": gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects1});
gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDdosObjects1Objects = Hashtable.newFrom({"dos": gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects1});
gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDipObjects1Objects = Hashtable.newFrom({"ip": gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects1});
gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDCROIXObjects1Objects = Hashtable.newFrom({"CROIX": gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects1});
gdjs._4990_32DOSSIERCYBERSECUCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("controle"), gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDcontroleObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "200 CONTROLE", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ext"), gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDextObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "210 DEF EXT", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("dos"), gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDdosObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "220 DEF DOS", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ip"), gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDipObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "230 DEF IP", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("CROIX"), gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4990_32DOSSIERCYBERSECUCode.mapOfGDgdjs_9546_95954990_959532DOSSIERCYBERSECUCode_9546GDCROIXObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "130", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4990_32DOSSIERCYBERSECUCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4990_32DOSSIERCYBERSECUCode.GDFondObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDFondObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDbackObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDbackObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDadresseObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDadresseObjects2.length = 0;

gdjs._4990_32DOSSIERCYBERSECUCode.eventsList0(runtimeScene);
gdjs._4990_32DOSSIERCYBERSECUCode.GDFondObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDFondObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDcontroleObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDextObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDdosObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDipObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDbackObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDbackObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDCROIXObjects2.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDadresseObjects1.length = 0;
gdjs._4990_32DOSSIERCYBERSECUCode.GDadresseObjects2.length = 0;


return;

}

gdjs['_4990_32DOSSIERCYBERSECUCode'] = gdjs._4990_32DOSSIERCYBERSECUCode;
