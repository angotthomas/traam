gdjs._4930Code = {};
gdjs._4930Code.localVariables = [];
gdjs._4930Code.GDFondObjects1= [];
gdjs._4930Code.GDFondObjects2= [];
gdjs._4930Code.GDtroisdimObjects1= [];
gdjs._4930Code.GDtroisdimObjects2= [];
gdjs._4930Code.GDsortiesObjects1= [];
gdjs._4930Code.GDsortiesObjects2= [];
gdjs._4930Code.GDcyberObjects1= [];
gdjs._4930Code.GDcyberObjects2= [];
gdjs._4930Code.GDadresseObjects1= [];
gdjs._4930Code.GDadresseObjects2= [];


gdjs._4930Code.mapOfGDgdjs_9546_95954930Code_9546GDtroisdimObjects1Objects = Hashtable.newFrom({"troisdim": gdjs._4930Code.GDtroisdimObjects1});
gdjs._4930Code.mapOfGDgdjs_9546_95954930Code_9546GDsortiesObjects1Objects = Hashtable.newFrom({"sorties": gdjs._4930Code.GDsortiesObjects1});
gdjs._4930Code.mapOfGDgdjs_9546_95954930Code_9546GDcyberObjects1Objects = Hashtable.newFrom({"cyber": gdjs._4930Code.GDcyberObjects1});
gdjs._4930Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("troisdim"), gdjs._4930Code.GDtroisdimObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4930Code.mapOfGDgdjs_9546_95954930Code_9546GDtroisdimObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "140", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sorties"), gdjs._4930Code.GDsortiesObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4930Code.mapOfGDgdjs_9546_95954930Code_9546GDsortiesObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "150", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("cyber"), gdjs._4930Code.GDcyberObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4930Code.mapOfGDgdjs_9546_95954930Code_9546GDcyberObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "190 DOSSIERCYBERSECU", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4930Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4930Code.GDFondObjects1.length = 0;
gdjs._4930Code.GDFondObjects2.length = 0;
gdjs._4930Code.GDtroisdimObjects1.length = 0;
gdjs._4930Code.GDtroisdimObjects2.length = 0;
gdjs._4930Code.GDsortiesObjects1.length = 0;
gdjs._4930Code.GDsortiesObjects2.length = 0;
gdjs._4930Code.GDcyberObjects1.length = 0;
gdjs._4930Code.GDcyberObjects2.length = 0;
gdjs._4930Code.GDadresseObjects1.length = 0;
gdjs._4930Code.GDadresseObjects2.length = 0;

gdjs._4930Code.eventsList0(runtimeScene);
gdjs._4930Code.GDFondObjects1.length = 0;
gdjs._4930Code.GDFondObjects2.length = 0;
gdjs._4930Code.GDtroisdimObjects1.length = 0;
gdjs._4930Code.GDtroisdimObjects2.length = 0;
gdjs._4930Code.GDsortiesObjects1.length = 0;
gdjs._4930Code.GDsortiesObjects2.length = 0;
gdjs._4930Code.GDcyberObjects1.length = 0;
gdjs._4930Code.GDcyberObjects2.length = 0;
gdjs._4930Code.GDadresseObjects1.length = 0;
gdjs._4930Code.GDadresseObjects2.length = 0;


return;

}

gdjs['_4930Code'] = gdjs._4930Code;
