gdjs._490Code = {};
gdjs._490Code.localVariables = [];
gdjs._490Code.GDFondObjects1= [];
gdjs._490Code.GDFondObjects2= [];
gdjs._490Code.GDNewTextObjects1= [];
gdjs._490Code.GDNewTextObjects2= [];


gdjs._490Code.mapOfGDgdjs_9546_9595490Code_9546GDFondObjects1Objects = Hashtable.newFrom({"Fond": gdjs._490Code.GDFondObjects1});
gdjs._490Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Fond"), gdjs._490Code.GDFondObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._490Code.mapOfGDgdjs_9546_9595490Code_9546GDFondObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "20", true);
}}

}


};

gdjs._490Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._490Code.GDFondObjects1.length = 0;
gdjs._490Code.GDFondObjects2.length = 0;
gdjs._490Code.GDNewTextObjects1.length = 0;
gdjs._490Code.GDNewTextObjects2.length = 0;

gdjs._490Code.eventsList0(runtimeScene);
gdjs._490Code.GDFondObjects1.length = 0;
gdjs._490Code.GDFondObjects2.length = 0;
gdjs._490Code.GDNewTextObjects1.length = 0;
gdjs._490Code.GDNewTextObjects2.length = 0;


return;

}

gdjs['_490Code'] = gdjs._490Code;
