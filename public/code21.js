gdjs._5000_32CONTROLECode = {};
gdjs._5000_32CONTROLECode.localVariables = [];
gdjs._5000_32CONTROLECode.GDFondObjects1= [];
gdjs._5000_32CONTROLECode.GDFondObjects2= [];
gdjs._5000_32CONTROLECode.GDOKObjects1= [];
gdjs._5000_32CONTROLECode.GDOKObjects2= [];
gdjs._5000_32CONTROLECode.GDAnnulerObjects1= [];
gdjs._5000_32CONTROLECode.GDAnnulerObjects2= [];
gdjs._5000_32CONTROLECode.GDNewTextInputObjects1= [];
gdjs._5000_32CONTROLECode.GDNewTextInputObjects2= [];
gdjs._5000_32CONTROLECode.GDadresseObjects1= [];
gdjs._5000_32CONTROLECode.GDadresseObjects2= [];


gdjs._5000_32CONTROLECode.mapOfGDgdjs_9546_95955000_959532CONTROLECode_9546GDOKObjects1Objects = Hashtable.newFrom({"OK": gdjs._5000_32CONTROLECode.GDOKObjects1});
gdjs._5000_32CONTROLECode.mapOfGDgdjs_9546_95955000_959532CONTROLECode_9546GDAnnulerObjects1Objects = Hashtable.newFrom({"Annuler": gdjs._5000_32CONTROLECode.GDAnnulerObjects1});
gdjs._5000_32CONTROLECode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("OK"), gdjs._5000_32CONTROLECode.GDOKObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._5000_32CONTROLECode.mapOfGDgdjs_9546_95955000_959532CONTROLECode_9546GDOKObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "200 CONTROLE", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Annuler"), gdjs._5000_32CONTROLECode.GDAnnulerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._5000_32CONTROLECode.mapOfGDgdjs_9546_95955000_959532CONTROLECode_9546GDAnnulerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "190 DOSSIERCYBERSECU", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._5000_32CONTROLECode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._5000_32CONTROLECode.GDFondObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDFondObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDOKObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDOKObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDAnnulerObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDAnnulerObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDNewTextInputObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDNewTextInputObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDadresseObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDadresseObjects2.length = 0;

gdjs._5000_32CONTROLECode.eventsList0(runtimeScene);
gdjs._5000_32CONTROLECode.GDFondObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDFondObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDOKObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDOKObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDAnnulerObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDAnnulerObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDNewTextInputObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDNewTextInputObjects2.length = 0;
gdjs._5000_32CONTROLECode.GDadresseObjects1.length = 0;
gdjs._5000_32CONTROLECode.GDadresseObjects2.length = 0;


return;

}

gdjs['_5000_32CONTROLECode'] = gdjs._5000_32CONTROLECode;
