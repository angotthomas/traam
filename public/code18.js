gdjs._4970_32PARISCode = {};
gdjs._4970_32PARISCode.localVariables = [];
gdjs._4970_32PARISCode.GDFondObjects1= [];
gdjs._4970_32PARISCode.GDFondObjects2= [];
gdjs._4970_32PARISCode.GDCroixObjects1= [];
gdjs._4970_32PARISCode.GDCroixObjects2= [];
gdjs._4970_32PARISCode.GDadresseObjects1= [];
gdjs._4970_32PARISCode.GDadresseObjects2= [];


gdjs._4970_32PARISCode.mapOfGDgdjs_9546_95954970_959532PARISCode_9546GDCroixObjects1Objects = Hashtable.newFrom({"Croix": gdjs._4970_32PARISCode.GDCroixObjects1});
gdjs._4970_32PARISCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Croix"), gdjs._4970_32PARISCode.GDCroixObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4970_32PARISCode.mapOfGDgdjs_9546_95954970_959532PARISCode_9546GDCroixObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "150", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4970_32PARISCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4970_32PARISCode.GDFondObjects1.length = 0;
gdjs._4970_32PARISCode.GDFondObjects2.length = 0;
gdjs._4970_32PARISCode.GDCroixObjects1.length = 0;
gdjs._4970_32PARISCode.GDCroixObjects2.length = 0;
gdjs._4970_32PARISCode.GDadresseObjects1.length = 0;
gdjs._4970_32PARISCode.GDadresseObjects2.length = 0;

gdjs._4970_32PARISCode.eventsList0(runtimeScene);
gdjs._4970_32PARISCode.GDFondObjects1.length = 0;
gdjs._4970_32PARISCode.GDFondObjects2.length = 0;
gdjs._4970_32PARISCode.GDCroixObjects1.length = 0;
gdjs._4970_32PARISCode.GDCroixObjects2.length = 0;
gdjs._4970_32PARISCode.GDadresseObjects1.length = 0;
gdjs._4970_32PARISCode.GDadresseObjects2.length = 0;


return;

}

gdjs['_4970_32PARISCode'] = gdjs._4970_32PARISCode;
