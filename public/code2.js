gdjs._510Code = {};
gdjs._510Code.localVariables = [];
gdjs._510Code.GDFondObjects1= [];
gdjs._510Code.GDFondObjects2= [];
gdjs._510Code.GDWifiObjects1= [];
gdjs._510Code.GDWifiObjects2= [];


gdjs._510Code.mapOfGDgdjs_9546_9595510Code_9546GDWifiObjects1Objects = Hashtable.newFrom({"Wifi": gdjs._510Code.GDWifiObjects1});
gdjs._510Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Wifi"), gdjs._510Code.GDWifiObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._510Code.mapOfGDgdjs_9546_9595510Code_9546GDWifiObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "40", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._510Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._510Code.GDFondObjects1.length = 0;
gdjs._510Code.GDFondObjects2.length = 0;
gdjs._510Code.GDWifiObjects1.length = 0;
gdjs._510Code.GDWifiObjects2.length = 0;

gdjs._510Code.eventsList0(runtimeScene);
gdjs._510Code.GDFondObjects1.length = 0;
gdjs._510Code.GDFondObjects2.length = 0;
gdjs._510Code.GDWifiObjects1.length = 0;
gdjs._510Code.GDWifiObjects2.length = 0;


return;

}

gdjs['_510Code'] = gdjs._510Code;
